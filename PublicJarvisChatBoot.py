###
#   █████╗ ██████╗ 
#  ██╔══██╗██╔══██╗
#  ███████║██████╔╝
#  ██╔══██║██╔═══╝
#  ██║  ██║██║  
#  ╚═╝  ╚═╝╚═╝  
#
# AP - Alessandro's Personal Assistant
#
# This code implements an interactive voice-based chatbot using OpenAI's GPT-4 model.
# The chatbot listens to the user's voice input, processes it, and generates a response
# based on the conversation context. It then converts the response to speech and plays
# it back to the user. The conversation can be saved to a text file or opened with a
# viewer. The chatbot is designed to act as a helpful assistant named Jarvis, assisting
# a superhero.
###
import openai
import speech_recognition as sr
from gtts import gTTS
import os
from pydub import AudioSegment
from pydub.playback import play
import subprocess

# Set up the OpenAI API key
openai.api_key = "ADD-YOUR-KEY"
# Set up language
SPEECH_TO_TEXT = "it-IT"
TEXT_TO_SPEECH = "it"
CHATBOT_NAME = "Jarvis"

def ask_gpt4_chat(messages, model="gpt-3.5-turbo", tokens=150, temperature=0.5):
    """
    Sends a list of messages to the GPT-4 model and receives a response.
    :param messages: List of messages with roles and content.
    :param model: GPT model to be used.
    :param tokens: Maximum number of tokens in the response.
    :param temperature: Controls randomness in the response.
    :return: A string containing the AI-generated message.
    """
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        max_tokens=tokens,
        n=1,
        stop=None,
        temperature=temperature,
        top_p=1
    )

    message = response.choices[0].message['content'].strip()
    return message

def messages_to_string(messages):
    """
    Converts a list of messages to a formatted string.
    :param messages: List of messages with roles and content.
    :return: A formatted string with role and content.
    """
    message_string = ""
    for message in messages:
        message_string += f"{message['role'].capitalize()}: {message['content']}\n"
    return message_string

def save_conversation(messages, filename="conversation_summary.txt"):
    """
    Saves the conversation to a text file.
    :param messages: List of messages with roles and content.
    :param filename: The name of the file to save the conversation.
    """
    conversation_string = messages_to_string(messages)
    with open(filename, "w") as f:
        f.write(conversation_string)

def open_conversation_with_viewer(filename="conversation_summary.txt"):
    """
    Opens the conversation text file with the default viewer.
    :param filename: The name of the file containing the conversation.
    """
    if os.path.isfile(filename):
        try:
            subprocess.run(["start", filename], check=True, shell=True)
        except subprocess.CalledProcessError as error:
            print(f"Error opening the file: {error}")
    else:
        print("Il file", filename, "File not found.")

def listen_for_user_input():
    """
    Listens to the user's voice input and returns the recognized text.
    :return: The recognized text as a string.
    """
    recognizer = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening...")
        audio = recognizer.listen(source, timeout=60)

    try:
        print("Recognizing...")
        text = recognizer.recognize_google(audio, language=SPEECH_TO_TEXT)
        return text
    except sr.UnknownValueError:
        print("Sorry, could not understand the audio")
        return ""
    except sr.RequestError as e:
        print("Could not request results; {0}".format(e))
        return ""

def play_response(response_text):
    """
    Converts the given text to speech and plays it.
    :param response_text: The text to be converted and played.
    """
    tts = gTTS(text=response_text, lang=TEXT_TO_SPEECH)
    tts.save("tmp_output.mp3")

    audio_file = AudioSegment.from_file("tmp_output.mp3")
    play(audio_file)

def talk_with_chatGPT(messages=None):
    """
    Handles the conversation with the ChatGPT AI by listening to user input,
    processing it, and responding using text-to-speech.
    :param messages: List of messages with roles and content.
    :return: Updated list of messages.
    """
    if messages is None:
        messages = []

    user_input = listen_for_user_input()

    triggeringActions(messages, user_input)

    print("You said:", user_input)

    # Send the recognized text to ChatGPT for processing
    messages.append({"role": "user", "content": user_input})
    ai_response = ask_gpt4_chat(messages)
    messages.append({"role": "assistant", "content": ai_response})
    response_text = ai_response

    print("ChatGPT:", response_text)

    # Convert the text to speech and play it back
    play_response(response_text)

    return messages

def triggeringActions(messages, user_input):
    """
    Triggers a set of actions responding to user commands.
    """
    actions = ["lorem ipsum"]
    
    if "salva la conversazione" in user_input.lower():
        print("You said: salva la conversazione")
        save_conversation(messages)
    if "apri la conversazione" in user_input.lower():
        print("You said: apri la conversazione")
        open_conversation_with_viewer()

if __name__ == "__main__":
    initial_messages = [
        {"role": "system", "content": "You are a helpful assistant called "+CHATBOT_NAME+"."}
    ]

    while True:
        talk_with_chatGPT(initial_messages)
