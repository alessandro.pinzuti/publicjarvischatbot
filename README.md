# JarvisChatBot

This code implements an interactive voice-based chatbot using OpenAI's GPT-4 model. The chatbot listens to the user's voice input, processes it, and generates a response based on the conversation context. It then converts the response to speech and plays it back to the user. The conversation can be saved to a text file or opened with a viewer. The chatbot is designed to act as a helpful assistant named Jarvis, assisting a superhero.

# Dependencies

- openai: This is the official OpenAI API client library for Python.
- speech_recognition: This is a library that provides easy-to-use speech recognition capabilities for Python.
- gtts: This is a library for Python that allows you to create speech from text.
- pydub: This is a Python library for working with audio files.

Installation Steps:

- Install openai using pip: pip install openai
- Install speech_recognition using pip: pip install SpeechRecognition
- Install gtts using pip: pip install gtts
- Install pydub using pip: pip install pydub

After installing these dependencies, you should be able to run the code in your project without any issues.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/alessandro.pinzuti/jarvischatbot.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment
The Author of this project is Alessandro Pinzuti

## License
The project code and its accompanying documentation are released under a license based on the terms of the MIT License. This means that anyone is free to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the project, subject to the conditions and limitations outlined in the original MIT License. Please see the included LICENSE file for the full text of the license.


## Project status
The project is currently in continuous development as we work to add new features, improve performance, and fix any issues that arise. 
